class UrlMappings {

	static mappings = {

        "/feedly/init"(controller: "feedly", action: "init")
        "/"(controller: "feedly", action: "callback")

        "/$controller/$action?/$id?(.$format)?"{
            constraints {
                // apply constraints here
            }
        }

//        "/"(view:"/index")
        "500"(view:'/error')
	}
}
