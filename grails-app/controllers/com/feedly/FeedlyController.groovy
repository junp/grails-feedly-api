package com.feedly

import com.feedly.client.FeedlyClient

import static org.springframework.http.HttpStatus.OK

class FeedlyController {

    def init() {
        FeedlyClient client = new FeedlyClient(
                [clientId: "sandbox", clientSecret: "A0SXFX54S3K0OC9GNCXG", sandbox: true])
        def callbackUrl = client.getCodeUrl("http://localhost:8080")
        redirect(url:callbackUrl)

        render OK
    }

    def callback() {
        if(!request.parameterMap.containsKey('code')) {
            render "This is callback url."
            return
        }

        def code = request.parameterMap.code[0].toString()
        def state = request.parameterMap.state[0].toString()
        println "====== code information ====="
        println "code:$code"
        println "state:$state"

        FeedlyClient client = new FeedlyClient(
                [clientId: "sandbox", clientSecret: "A0SXFX54S3K0OC9GNCXG", sandbox: true])
        def resAccessToken = client.getAccessToken("http://localhost:8080", code)
        def id = resAccessToken.id
        def accessToken = resAccessToken.access_token
        def refreshToken = resAccessToken.refresh_token
        def expiresIn = resAccessToken.expires_in
        println "===== token information ====="
        println "id: ${id}"
        println "access_token: ${accessToken}"
        println "refresh_token: ${refreshToken}"
        println "expires_in: ${expiresIn}"
        println "provider: ${resAccessToken.provider}"
        println "token_type: ${resAccessToken.token_type}"
        println "plan: ${resAccessToken.plan}"

        def subscriptions = client.getUserSubscriptions(accessToken)
        def streamId = subscriptions[0].categories[0].id
        def streams = client.getStreamContents(accessToken, streamId, 2)
        def contents = []
        streams.items.each{
            contents << it
        }
        while(streams.continuation) {
            println "continuation: ${streams.continuation}"
            streams = client.getStreamContentsWithContinuation(accessToken, streamId, streams.continuation, 2)
            streams.items.each{
                contents << it
            }
        }
        println "contents' size: ${contents.size()}"
        contents.each {
            println "======== content ========"
            println "- content: ${it.summary.content}"
            println "- direction: ${it.summary.direction}"
            println "- keywords: ${it.keywords}"
            println "- unread: ${it.unread}"
            println "- crawled: ${it.crawled}"
            println "- author: ${it.author}"
            println "- streamId: ${it.origin.streamId}"
            println "- htmlUrl: ${it.origin.htmlUrl}"
            println "- origin title: ${it.origin.title}"
            println "- alternate: ${it.alternate}"
            println "- published: ${it.published}"
            println "- title: ${it.title}"
            println "- origin id: ${it.originId}"
            println "- fingerprint: ${it.fingerprint}"
            println "- id: ${it.id}"
            println "- categories: ${it.categories}"
        }
        render contents
    }

}
