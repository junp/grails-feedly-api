package com.feedly.client

import grails.plugins.rest.client.RestBuilder;

class FeedlyClient {
    String clientId
    String clientSecret
    boolean sandbox
    String serviceHost

    FeedlyClient(def map) {
        this.clientId = map.clientId
        this.clientSecret = map.clientSecret
        this.sandbox = map.sandbox
        if(this.sandbox) {
            this.serviceHost = 'sandbox.feedly.com'
        } else {
            this.serviceHost = 'cloud.feedly.com'
        }
    }

    def getCodeUrl(String callbackUrl) {
        def scope = 'https://cloud.feedly.com/subscriptions'
        def responseType = 'code'

        return "${getEndpoint("v3/auth/auth")}?client_id=${this.clientId}&redirect_uri=${callbackUrl}&scope=$scope&response_type=$responseType"
    }

    def getAccessToken(String redirectUri, String codeStr) {
        return new RestBuilder().post(getEndpoint("v3/auth/token")) {
            client_id = this.clientId
            client_secret = this.clientSecret
            grant_type = "authorization_code"
            redirect_uri = redirectUri
            code = codeStr
        }.json
    }

    def refreshAccessToken(String refreshToken) {
        return new RestBuilder().post(getEndpoint("v3/auth/token")) {
            refresh_token = refreshToken
            client_id = this.clientId
            client_secret = this.clientSecret
            grant_type = "refresh_token"
        }.json
    }

    def getUserSubscriptions(String token) {
        return new RestBuilder().get(getEndpoint("v3/subscriptions")) {
            header 'Authorization', 'OAuth '+token
        }.json
    }

    def getStreamIds(String token, String id, int count = 20) {
        return new RestBuilder().get(getEndpoint("v3/streams/ids?streamId=$id&count=$count")) {
            header 'Authorization', 'OAuth '+token
        }.json
    }

    def getStreamContents(String token, String id, int count = 20) {
        return new RestBuilder().get(getEndpoint("v3/streams/contents?streamId=$id&count=$count")) {
            header 'Authorization', 'OAuth '+token
        }.json
    }

    def getStreamContentsWithContinuation(String token, String id, String continuation, int count = 20) {
        return new RestBuilder().get(getEndpoint("v3/streams/contents?streamId=$id&count=$count&continuation=$continuation")) {
            header 'Authorization', 'OAuth '+token
        }.json
    }

    def getEndpoint(String path = "") {
        return "https://${this.serviceHost}/${path}"
    }
}


